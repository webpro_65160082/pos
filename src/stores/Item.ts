import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { ItemList } from '@/types/ItemList'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAutshStore } from './auth'
import { useMemberStore } from './member'

export const userItemStore = defineStore('counter', () => {
    const authStore = useAutshStore()
    const memberStore = useMemberStore()
    const receiptDialog = ref(false)
  const receipt = ref<Receipt>({
      id: 0,
      createdDate: new Date(),
      total: 0,
      totalNet: 0,
      memberDiscount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.currentUser.id,
      user:authStore.currentUser,
      memberId: 0
  })
    const itemLists = ref<ItemList[]>([])
  function addItem(product: Product) {
      const index = itemLists.value.findIndex((item) => item.product?.id === product.id)
      if (index >= 0) {
          itemLists.value[index].unit++
          calculate()
          return
      } else {
          const newItem: ItemList = {
              id: -1,
              name: product.name,
              unit: 1,
              price: product.price,
              productId: product.id,
              product: product
          }
          itemLists.value.push(newItem)
          calculate()
      }
  }
  function removeItem(itemList: ItemList) {
      const index = itemLists.value.findIndex((item) => item === itemList)
      itemLists.value.splice(index, 1)
      calculate()
  }
  function inc(item: ItemList) {
      item.unit++
      calculate()
  }
  function deinc(item: ItemList) {
  
      if (item.unit === 1) {
          removeItem(item)
      }
      item.unit--
      calculate()
  }


function calculate(){
    let total = 0
    let memberDiscount = 0 
    for( const item of itemLists.value){
        total = total + (item.price * item.unit)
    }
    receipt.value.total= total
    if(memberStore.currentMember){
        memberDiscount = total - (total*0.95)
        receipt.value.memberDiscount = memberDiscount
    receipt.value.totalNet = total*0.95
}else{
        receipt.value.totalNet = total
    }
  }


  function billDialog() {
    receipt.value.itemLists = itemLists.value
    receiptDialog.value = true
    
}

function clear(){
    itemLists.value=[]
    receipt.value = {
        id: 0,
      createdDate: new Date(),
      total: 0,
      totalNet: 0,
      memberDiscount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: authStore.currentUser.id,
      user:authStore.currentUser,
      memberId: 0
    }
    memberStore.clear()
}

  return {
    itemLists,receipt,receiptDialog,
    addItem,removeItem,inc,deinc,calculate,billDialog,clear
}
})
