import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'


export const useAutshStore = defineStore('auth', () => {
    const currentUser = ref<User>(
        {id: 1, email: 'mana@gmailm.com',password:'Pass_1234',name:'Mana Manee',gender:'male',roles:['user']}
    )

  return { currentUser }
})
