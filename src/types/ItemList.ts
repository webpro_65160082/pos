import type { Product } from "./Product"
type ItemList = {
    id:number
    name:string
    unit: number
    price: number
    productId:number
    product?: Product
}
export { type ItemList }