import type { ItemList } from "./ItemList";
import type { Member } from "./Member";
import type { User } from "./User";

type Receipt = {
    id:number
    createdDate:Date;
    total: number;
    totalNet: number;
    memberDiscount:number;
    receivedAmount:number;
    change: number;
    paymentType: string;
    userId:number;
    user?:User;
    memberId:number;
    member?: Member;
    itemLists?:ItemList[]
}
export type { Receipt }